from django.shortcuts import render
from lab_3.forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect

# Create your views here.
@login_required (login_url = '/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # Mengambil seluruh object dari class Friend
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required (login_url = '/admin/login/')
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None)
      
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-3') 
  
    context['form']= form
    return render(request, "lab3_form.html", context)