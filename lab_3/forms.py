# import form class from django
from django import forms
  
# import Friend from lab1_.models.py
from lab_1.models import Friend
  
# create a ModelForm
class FriendForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Friend
        fields = "__all__"