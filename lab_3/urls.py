from django.urls import path
from .views import index
from .views import add_friend

urlpatterns = [
    path('', index, name = 'index'),
    # TODO Add friends path using friend_list Views
    path('add', add_friend, name='add_friend'),
]