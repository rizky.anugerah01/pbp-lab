### 1. Apakah perbedaan antara JSON dan XML? ###
-------------------------------------------------
Sebelum membahas perbedaannya, kita harus mengetahui apa pengertian dari JSON dan XML terlebih dahulu. JSON (JavaScript Object Notation) merupakan format pertukaran data yang ringan dan sepenuhnya tidak bergantung kepada language-nya. Hal didasarkan pada bahasa JavaScript yang sifatnya mudah dimengerti. Sedangkan XML (Extensible Markup Language) merupakan bahasa markup yang mendefinisikan seperangkat aturan untuk menyandikan dokumen dalam format yang dapat dibaca manusia dan dapat dibaca mesin. XML dirancang untuk membawa data, bukan untuk menampilkan data. Tujuan desain XML fokus pada kesederhanaan, umum, dan kegunaan di Internet. Meski desain XML berfokus pada dokumen, bahasa ini banyak digunakan untuk representasi struktur data arbitrer seperti yang digunakan dalam layanan web.
Berikut perbedaan antara JSON dan XML:
    * JSON merupakan objek dari JavaScript, sedangkan XML merupakan bahasa markup yang dapat diperluas.
    * JSON didasarkan pada bahasa JavaScript, sedangkan XML berasal dari SGML (Standard Generalized Markup Language).
    * JSON adalah cara untuk merepresentasikan objek, sedangkan XML adalah bahasa markup serta menggunakan struktur tag untuk mewakili item data.
    * JSON tidak mendukung untuk memberikan ruang nama, sedangkan XML mendukung penggunaan ruang nama.
    * JSON mendukung penggunaan array, sedangkan XML tidak
    * JSON memiliki file yang mudah dibaca, sedangkan XML memiliki dokumen yang cukup sulit untuk dibaca.
    * JSON tidak menggunakan tag akhir, sedangkan XML memiliki tag awal dan juga tag akhir.
    * JSON memiliki tingkat keamanan yang kurang dibandingkan dengan XML yang memiliki tingkat keamanan yang jauh lebih baik.
    * JSON tidak mendukung untuk memberi comments, sedangkan XML mendukung untuk pemberian comments
    * JSON hanya mendukung pengkodean dari UTF-8, sedangkan XML mendukung berbagai macam pengkodean.

### 2. Apakah perbedaan antara HTML dan XML? ###
-------------------------------------------------
HTML (Hyper Text Markup Language) merupakan bahasa markup yang digunakan untuk membuat halaman web dan aplikasi web secara umumnya digunakan untuk menampilkan data. HTML adalah kombinasi dari Hypertext dan bahasa Markup. Hypertext mendefinisikan link antara halaman web. Bahasa markup digunakan untuk mendefinisikan dokumen teks dalam tag yang mendefinisikan struktur halaman web. Bahasa ini digunakan untuk membuat catatan untuk komputer teks sehingga mesin dapat memahaminya dan memanipulasi teks yang sesuai
Berikut perbedaan antara HTML dan XML:
    * HTML bersifat statis, sedangkan XML dinamis.
    * HTML adalah bahasa markup, sedangkan XML menyediakan framework untuk mendefinisikan bahasa markup.
    * HTML dapat mengabaikan kesalahan kecil dan peka terhadap huruf kapital, sedangkan XML tidak mengizinkan jika terjadi kesalahan dan peka terhadap huruf kapital.
    * Tag HTML adalah tag yang telah ditentukan sebelumnya serta memiliki jumlah yang terbatas, sedangkan tag XML ditentukannya oleh pengguna serta dapat diperluas.
    * HTML tidak memperhatikan white spaces, sedangkan pada XML white spaces bisa diperhatikan.
    * Tag HTML digunakan untuk menampilkan data, sedangkan tag XML digunakan untuk mendeskripsikan data bukan untuk ditampilkan.
    * Pada HTML tag penutup tidak diperlukan, sedangkan pada XML tidak diperlukan.
    * HTML tidak membawa data tetapi hanya menampilkannya, sedangkan XML membawa data dan dari database
    
    Referensi:
    1. https://www.geeksforgeeks.org/difference-between-json-and-xml/
    2. https://www.geeksforgeeks.org/html-vs-xml/ 