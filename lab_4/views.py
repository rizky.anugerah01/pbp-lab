from django.shortcuts import render

# Create your views here.
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect


# Create your views here.
def index(request): # sesuai dengan views.py lab 2
    notes = Note.objects.all().values()  # Mengambil seluruh object dari class Note
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None)
      
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4') 
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)