from django.db import models
from django.utils import timezone
from datetime import datetime, date

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    # TODO Implement missing attributes in Friend model
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    dob = models.DateField()
